package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;
import tb.antlr.symbolTable.*;


public class MyTreeParser extends TreeParser {
	
	private LocalSymbols localSymbols = new LocalSymbols();

    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	
	protected Integer mod(Integer num1, Integer num2) {
		return num1%num2;
	}
	
	protected Integer pow(Integer base, Integer exponent) {
		return (int) Math.pow(base, exponent);
	}
	
	protected Integer div(Integer num1, Integer num2) {
		if(num2 == 0) {
			throw new IllegalArgumentException("num2 is 0"); 
		} 
			return num1/num2;
	}
	
	protected void enterScope() {
		 localSymbols.enterScope();
	}
	
	protected void leaveScope() {
		localSymbols.leaveScope();
	}
	
	// wyjatki obslugiwane sa juz w funkcjach klasy LocalSymbols
	protected void declareVar(String varName) {
		localSymbols.newSymbol(varName);
	}
	
	protected void setVar(String varName, Integer value) {
		localSymbols.setSymbol(varName, value);
	}
	
	protected Integer getID(String varName) {
		return localSymbols.getSymbol(varName);
	}
}

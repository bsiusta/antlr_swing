tree grammar TExpr1;

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
}

prog    : (expr)* ;

expr returns [Integer out]
	      : ^(PLUS  e1=expr e2=expr) {$out = $e1.out + $e2.out;}
        | ^(MINUS e1=expr e2=expr) {$out = $e1.out - $e2.out;}
        | ^(MUL   e1=expr e2=expr) {$out = $e1.out * $e2.out;}
        | ^(DIV   e1=expr e2=expr) {$out = div($e1.out, $e2.out);}
        | ^(MOD e1=expr e2= expr) {$out = mod($e1.out, $e2.out);}
        | ^(POW e1=expr e2=expr) {$out = pow($e1.out, $e2.out);}
        | ^(PRINT e=expr) {drukuj ($e.text + " = " + $e.out.toString());}  
        | INT                      {$out = getInt($INT.text);}
        | ^(VAR i1=ID) {declareVar($i1.text);}
        | ^(PODST i1=ID   e2=expr) {setVar($i1.text, $e2.out);}    
        | ID {$out = getID($ID.text);}
        | ES {enterScope();}
        | LS {leaveScope();}
        ;

 